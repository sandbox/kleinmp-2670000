<?php

/**
 * @file
 * Contains \Drupal\colresizable\Element\ColResizableTable.
 */

namespace Drupal\colresizable\Element;

use Drupal\Component\Utility\Html;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\Table;

/**
 * Provides a form element for a resizable table.
 *
 * Properties:
 * - #header: Table headers used in the table.
 * - #options: An associative array where each key is the value returned when
 *   a user selects the radio button or checkbox, and each value is the row of
 *   table data.
 *
 * @see \Drupal\Core\Render\Element\Table
 *
 * @RenderElement("colresizable_table")
 */
class ColResizableTable extends Table {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    $info = parent::getInfo();

    $info['#process'][] = array($class, 'processColResizableTable');
    $info['#colresizable'] = FALSE;

    return $info;
  }

  /**
   *
   */
  /**
   * #process callback for #type 'colresizable_table' the jquery library support.
   *
   *  Note that this is also used for table and tableselect.
   *
   * @param array $element
   *   An associative array containing the properties and children of the
   *   table element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed element.
   */
  public static function processColResizableTable(&$element, $form_state, &$complete_form) {
    if (!empty($element['#colresizable'])) {
      $element['#attached']['library'][] = 'colresizable/colresizable.module';
      $element['#attributes']['class'][] = 'colresizable';
    }
    return $element;
  }
}
