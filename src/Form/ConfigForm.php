<?php

/**
 * @file
 * Contains Drupal\colresizable\Form\ConfigForm.
 */

namespace Drupal\colresizable\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ConfigForm.
 *
 * @package Drupal\colresizable\Form
 */
class ConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'colresizable.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('colresizable.settings');

    // @todo: add version options?
    $form['js_on_all_pages'] = array(
      '#title' => t('Add colResizable to All Pages'),
      '#description' => t('Add the colResizable javascript file to all pages on the site. This makes it available to any table that uses the "colresizable" class.'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('js_on_all_pages'),
    );
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $config = $this->config('colresizable.settings');
    $config->set('js_on_all_pages', $form_state->getValue('js_on_all_pages'));
    $config->save();
  }

}
