(function ($) {
  Drupal.behaviors.colResizable = {
    attach: function (context, settings) {
      $('table.colresizable').once('colresizable').colResizable({
        liveDrag: true
      });
    }
  };
})(jQuery);
